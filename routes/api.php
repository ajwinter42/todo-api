<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('tasks', function () {
    return new \Illuminate\Http\JsonResponse([
       ['title' => 'Task A', 'description' => 'description of task a'],
       ['title' => 'Task B', 'description' => 'description of task b'],
       ['title' => 'Task C', 'description' => 'description of task c'],
       ['title' => 'Task D', 'description' => 'description of task d'],
    ]);
});
